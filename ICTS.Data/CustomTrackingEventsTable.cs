//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ICTS.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomTrackingEventsTable
    {
        public int Id { get; set; }
        public System.Guid WorkflowInstanceId { get; set; }
        public Nullable<long> RecordNumber { get; set; }
        public Nullable<byte> TraceLevelId { get; set; }
        public string CustomRecordName { get; set; }
        public string ActivityName { get; set; }
        public string ActivityId { get; set; }
        public string ActivityInstanceId { get; set; }
        public string ActivityType { get; set; }
        public string SerializedData { get; set; }
        public string SerializedAnnotations { get; set; }
        public System.DateTime TimeCreated { get; set; }
    }
}
