//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ICTS.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class WFTask
    {
        public System.Guid Id { get; set; }
        public string TaskName { get; set; }
        public Nullable<System.Guid> WFProjectId { get; set; }
        public string Actor { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }
    
        public virtual WFProject WFProject { get; set; }
    }
}
