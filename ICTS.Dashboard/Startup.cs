﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ICTS.Dashboard.Startup))]
namespace ICTS.Dashboard
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
