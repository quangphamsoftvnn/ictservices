﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ICTS.Data;
namespace ICTS.Dashboard.Models
{
    public class UserProfileWFModel
    {
        public UserProfile userProfile { get; set; }
        public WFProject projectWF { get; set; }
        public IList<WFTask> taskWF { get; set; }
        public IList<WFVariableInstance> variableWF { get; set; }
        public WFVersion versionWF { get; set; }
        public IList<Group> group { get; set; }
        public Role role { get; set; }
        public WFTask wfTask {get ; set;}
        public WFProject wfProject { get; set; }
    }
}