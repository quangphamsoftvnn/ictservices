﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ICTS.Data;

namespace ICTS.Dashboard.Controllers
{
    public class UserProfileRoleController : Controller
    {
        private ICTSDashboardData db = new ICTSDashboardData();

        // GET: /UserProfileRole/
        public ActionResult Index()
        {
            var profile_role = db.Profile_Role.Include(p => p.Role).Include(p => p.UserProfile);
            return View(profile_role.ToList());
        }

        // GET: /UserProfileRole/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Profile_Role profile_role = db.Profile_Role.Find(id);
            if (profile_role == null)
            {
                return HttpNotFound();
            }
            return View(profile_role);
        }

        // GET: /UserProfileRole/Create
        public ActionResult Create()
        {
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name");
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "FrirstName");
            return View();
        }

        // POST: /UserProfileRole/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,RoleId,UserProfileId,DateCreate,DateUpdate")] Profile_Role profile_role)
        {
            if (ModelState.IsValid)
            {
                profile_role.Id = Guid.NewGuid();
                db.Profile_Role.Add(profile_role);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name", profile_role.RoleId);
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "FrirstName", profile_role.UserProfileId);
            return View(profile_role);
        }

        // GET: /UserProfileRole/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Profile_Role profile_role = db.Profile_Role.Find(id);
            if (profile_role == null)
            {
                return HttpNotFound();
            }
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name", profile_role.RoleId);
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "FrirstName", profile_role.UserProfileId);
            return View(profile_role);
        }

        // POST: /UserProfileRole/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,RoleId,UserProfileId,DateCreate,DateUpdate")] Profile_Role profile_role)
        {
            if (ModelState.IsValid)
            {
                db.Entry(profile_role).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name", profile_role.RoleId);
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "FrirstName", profile_role.UserProfileId);
            return View(profile_role);
        }

        // GET: /UserProfileRole/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Profile_Role profile_role = db.Profile_Role.Find(id);
            if (profile_role == null)
            {
                return HttpNotFound();
            }
            return View(profile_role);
        }

        // POST: /UserProfileRole/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Profile_Role profile_role = db.Profile_Role.Find(id);
            db.Profile_Role.Remove(profile_role);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
