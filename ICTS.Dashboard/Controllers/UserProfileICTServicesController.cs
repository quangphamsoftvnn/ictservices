﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ICTS.Data;

namespace ICTS.Dashboard.Controllers
{
    public class UserProfileICTServicesController : Controller
    {
        private ICTSDashboardData db = new ICTSDashboardData();

        // GET: /UserProfileICTServices/
        public ActionResult Index()
        {
            var userprofile_ictservice = db.UserProfile_ICTService.Include(u => u.ICTService).Include(u => u.UserProfile);
            return View(userprofile_ictservice.ToList());
        }

        // GET: /UserProfileICTServices/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile_ICTService userprofile_ictservice = db.UserProfile_ICTService.Find(id);
            if (userprofile_ictservice == null)
            {
                return HttpNotFound();
            }
            return View(userprofile_ictservice);
        }

        // GET: /UserProfileICTServices/Create
        public ActionResult Create()
        {
            ViewBag.ICTServiceId = new SelectList(db.ICTServices, "Id", "NameService");
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "FrirstName");
            return View();
        }

        // POST: /UserProfileICTServices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,UserProfileId,ICTServiceId,Status,DateRequest")] UserProfile_ICTService userprofile_ictservice)
        {
            if (ModelState.IsValid)
            {
                userprofile_ictservice.Id = Guid.NewGuid();
                db.UserProfile_ICTService.Add(userprofile_ictservice);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ICTServiceId = new SelectList(db.ICTServices, "Id", "NameService", userprofile_ictservice.ICTServiceId);
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "FrirstName", userprofile_ictservice.UserProfileId);
            return View(userprofile_ictservice);
        }

        // GET: /UserProfileICTServices/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile_ICTService userprofile_ictservice = db.UserProfile_ICTService.Find(id);
            if (userprofile_ictservice == null)
            {
                return HttpNotFound();
            }
            ViewBag.ICTServiceId = new SelectList(db.ICTServices, "Id", "NameService", userprofile_ictservice.ICTServiceId);
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "FrirstName", userprofile_ictservice.UserProfileId);
            return View(userprofile_ictservice);
        }

        // POST: /UserProfileICTServices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,UserProfileId,ICTServiceId,Status,DateRequest")] UserProfile_ICTService userprofile_ictservice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userprofile_ictservice).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ICTServiceId = new SelectList(db.ICTServices, "Id", "NameService", userprofile_ictservice.ICTServiceId);
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "FrirstName", userprofile_ictservice.UserProfileId);
            return View(userprofile_ictservice);
        }

        // GET: /UserProfileICTServices/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile_ICTService userprofile_ictservice = db.UserProfile_ICTService.Find(id);
            if (userprofile_ictservice == null)
            {
                return HttpNotFound();
            }
            return View(userprofile_ictservice);
        }

        // POST: /UserProfileICTServices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            UserProfile_ICTService userprofile_ictservice = db.UserProfile_ICTService.Find(id);
            db.UserProfile_ICTService.Remove(userprofile_ictservice);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
