﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ICTS.Data;

namespace ICTS.Dashboard.Controllers
{
    public class RoleGroupController : Controller
    {
        private ICTSDashboardData db = new ICTSDashboardData();

        // GET: /RoleGroup/
        public ActionResult Index()
        {
            var role_group = db.Role_Group.Include(r => r.Group).Include(r => r.Role);
            return View(role_group.ToList());
        }

        // GET: /RoleGroup/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Role_Group role_group = db.Role_Group.Find(id);
            if (role_group == null)
            {
                return HttpNotFound();
            }
            return View(role_group);
        }

        // GET: /RoleGroup/Create
        public ActionResult Create()
        {
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name");
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name");
            return View();
        }

        // POST: /RoleGroup/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,RoleId,GroupId,DateCreate,DateUpdate")] Role_Group role_group)
        {
            if (ModelState.IsValid)
            {
                role_group.Id = Guid.NewGuid();
                db.Role_Group.Add(role_group);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", role_group.GroupId);
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name", role_group.RoleId);
            return View(role_group);
        }

        // GET: /RoleGroup/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Role_Group role_group = db.Role_Group.Find(id);
            if (role_group == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", role_group.GroupId);
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name", role_group.RoleId);
            return View(role_group);
        }

        // POST: /RoleGroup/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,RoleId,GroupId,DateCreate,DateUpdate")] Role_Group role_group)
        {
            if (ModelState.IsValid)
            {
                db.Entry(role_group).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", role_group.GroupId);
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Name", role_group.RoleId);
            return View(role_group);
        }

        // GET: /RoleGroup/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Role_Group role_group = db.Role_Group.Find(id);
            if (role_group == null)
            {
                return HttpNotFound();
            }
            return View(role_group);
        }

        // POST: /RoleGroup/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Role_Group role_group = db.Role_Group.Find(id);
            db.Role_Group.Remove(role_group);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
