﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ICTS.Data;

namespace ICTS.Dashboard.Controllers
{
    public class ICTServiceController : Controller
    {
        private ICTSDashboardData db = new ICTSDashboardData();

        // GET: /ICTService/
        public ActionResult Index()
        {
            return View(db.ICTServices.ToList());
        }

        // GET: /ICTService/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ICTService ictservice = db.ICTServices.Find(id);
            if (ictservice == null)
            {
                return HttpNotFound();
            }
            return View(ictservice);
        }

        // GET: /ICTService/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /ICTService/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,NameService,Status")] ICTService ictservice)
        {
            if (ModelState.IsValid)
            {
                ictservice.Id = Guid.NewGuid();
                db.ICTServices.Add(ictservice);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ictservice);
        }

        // GET: /ICTService/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ICTService ictservice = db.ICTServices.Find(id);
            if (ictservice == null)
            {
                return HttpNotFound();
            }
            return View(ictservice);
        }

        // POST: /ICTService/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,NameService,Status")] ICTService ictservice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ictservice).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ictservice);
        }

        // GET: /ICTService/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ICTService ictservice = db.ICTServices.Find(id);
            if (ictservice == null)
            {
                return HttpNotFound();
            }
            return View(ictservice);
        }

        // POST: /ICTService/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            ICTService ictservice = db.ICTServices.Find(id);
            db.ICTServices.Remove(ictservice);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
