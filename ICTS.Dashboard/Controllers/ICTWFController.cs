﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ICTS.Data;
using ICTS.Bussiness.UserProfileManage;
using ICTS.Dashboard.Models;

namespace ICTS.Dashboard.Controllers
{
    public class ICTWFController : Controller
    {
        private ICTSDashboardData dbContext = new ICTSDashboardData();

        // GET: /ICTWF/
        public ActionResult Index()
        {

            return View(dbContext.UserProfiles.ToList());

        }

        // GET: /ICTWF/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile userprofile = dbContext.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        // GET: /ICTWF/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /ICTWF/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,FrirstName,LastName,MidName,LoginName,Password,Email,Telephone,Status,DateCreate,DateUpdate")] UserProfile userprofile)
        {
            if (ModelState.IsValid)
            {
                userprofile.Id = Guid.NewGuid();
                dbContext.UserProfiles.Add(userprofile);
                dbContext.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(userprofile);
        }

        // GET: /ICTWF/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile userprofile = dbContext.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        // POST: /ICTWF/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,FrirstName,LastName,MidName,LoginName,Password,Email,Telephone,Status,DateCreate,DateUpdate")] UserProfile userprofile)
        {
            if (ModelState.IsValid)
            {
                dbContext.Entry(userprofile).State = EntityState.Modified;
                dbContext.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userprofile);
        }

        // GET: /ICTWF/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile userprofile = dbContext.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        // POST: /ICTWF/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            UserProfile userprofile = dbContext.UserProfiles.Find(id);
            dbContext.UserProfiles.Remove(userprofile);
            dbContext.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbContext.Dispose();
            }
            base.Dispose(disposing);
        }


        // GET: /ICTWF/
        public ActionResult CreateUserWF()
        { 
            return View(); 
        }

        // POST: /ICTWF/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUserWF([Bind(Include = "Id,FrirstName,LastName,MidName,LoginName,Password,Email,Telephone,Status,DateCreate,DateUpdate")] UserProfile userprofile)
        {
            if (ModelState.IsValid)
            {
                userprofile.Id = Guid.NewGuid();

                IUserProfileManage userManage = new UserProfileManage();
                WFProject project =  userManage.CreateUserProfileWF(userprofile);

                return RedirectToAction("ManagerWF", new { 
                    id = userprofile.Id,
                    projectid = project.Id,
                    instanceId = project.InstanceId
                });
            } 
            return View(userprofile);
        }

        // GET: /ICTWF/Details/5
        public ActionResult ManagerWF(Guid? id, Guid? projectid, Guid? instanceId)
        {
            UserProfileWFModel model = new UserProfileWFModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile userprofile = dbContext.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            model.userProfile = userprofile;

            IList<WFTask> tasks = dbContext.WFTasks.Where(t => t.WFProjectId == projectid).ToList<WFTask>();

            model.taskWF = tasks;

            return View(model);
        }

        // GET: /ICTWF/Details/5
        public ActionResult ValidateTask(Guid? id, Guid? taskid)
        {
            UserProfileWFModel model = new UserProfileWFModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile userprofile = dbContext.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            model.userProfile = userprofile;

            WFTask task = dbContext.WFTasks.Find(taskid);

            model.wfTask = task;

            return View(task.TaskName, model);
        }

    }
}
