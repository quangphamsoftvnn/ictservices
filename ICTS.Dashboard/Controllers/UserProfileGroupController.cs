﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ICTS.Data;

namespace ICTS.Dashboard.Controllers
{
    public class UserProfileGroupController : Controller
    {
        private ICTSDashboardData db = new ICTSDashboardData();

        // GET: /UserProfileGroup/
        public ActionResult Index()
        {
            var userprofile_group = db.UserProfile_Group.Include(u => u.Group).Include(u => u.UserProfile);
            return View(userprofile_group.ToList());
        }

        // GET: /UserProfileGroup/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile_Group userprofile_group = db.UserProfile_Group.Find(id);
            if (userprofile_group == null)
            {
                return HttpNotFound();
            }
            return View(userprofile_group);
        }

        // GET: /UserProfileGroup/Create
        public ActionResult Create()
        {
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name");
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "FrirstName");
            return View();
        }

        // POST: /UserProfileGroup/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,UserProfileId,GroupId,UpdateDate,CreateDate")] UserProfile_Group userprofile_group)
        {
            if (ModelState.IsValid)
            {
                userprofile_group.Id = Guid.NewGuid();
                db.UserProfile_Group.Add(userprofile_group);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", userprofile_group.GroupId);
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "FrirstName", userprofile_group.UserProfileId);
            return View(userprofile_group);
        }

        // GET: /UserProfileGroup/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile_Group userprofile_group = db.UserProfile_Group.Find(id);
            if (userprofile_group == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", userprofile_group.GroupId);
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "FrirstName", userprofile_group.UserProfileId);
            return View(userprofile_group);
        }

        // POST: /UserProfileGroup/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,UserProfileId,GroupId,UpdateDate,CreateDate")] UserProfile_Group userprofile_group)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userprofile_group).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", userprofile_group.GroupId);
            ViewBag.UserProfileId = new SelectList(db.UserProfiles, "Id", "FrirstName", userprofile_group.UserProfileId);
            return View(userprofile_group);
        }

        // GET: /UserProfileGroup/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile_Group userprofile_group = db.UserProfile_Group.Find(id);
            if (userprofile_group == null)
            {
                return HttpNotFound();
            }
            return View(userprofile_group);
        }

        // POST: /UserProfileGroup/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            UserProfile_Group userprofile_group = db.UserProfile_Group.Find(id);
            db.UserProfile_Group.Remove(userprofile_group);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
