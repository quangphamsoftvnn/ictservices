﻿using ICTS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ICTS.Dashboard.Controllers
{
    public class HomeController : Controller
    {
        private ICTSDashboardData db = new ICTSDashboardData();

        // GET: /UserProfile/
        public ActionResult Index()
        {
            return View(db.UserProfiles.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}