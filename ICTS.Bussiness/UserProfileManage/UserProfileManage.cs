﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICTS.Data;
using ICTS.Bussiness.WF;

namespace ICTS.Bussiness.UserProfileManage
{
    public class UserProfileManage : IUserProfileManage
    {
        private ICTSDashboardData db = new ICTSDashboardData();
        public UserProfileManage()
        {

        }

        public WFProject CreateUserProfileWF(Data.UserProfile userprofile)
        {
            db.UserProfiles.Add(userprofile);
            IWFManage wFMange = new WFManage();
            WFProject project =  wFMange.StartWorkFlow(userprofile);
            db.SaveChanges();

            return project;
        }
    }
}
