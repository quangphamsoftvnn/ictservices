﻿using ICTS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICTS.Bussiness.UserProfileManage
{
    public interface IUserProfileManage
    {

        WFProject CreateUserProfileWF(Data.UserProfile userprofile);
    }
}
