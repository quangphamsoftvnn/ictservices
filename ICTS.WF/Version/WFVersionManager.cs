﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICTS.Data;
using System.Activities.DynamicUpdate;
using System.Activities;
using System.Configuration;
using System.IO;
using System.Xaml;
using System.Runtime.Serialization;
using System.Reflection;
using System.Activities.XamlIntegration;

namespace ICTS.WF.Version
{
    public class WFVersionManager
    {
        public WFVersionManager()
        {

        }
         
        private ICTSDashboardData dt = new ICTSDashboardData();

        public void SaveUploadWorkflow(string workflowName, string SaaSParam, string saveAdress, string workflowLevel)
        {
            var maxValuae = dt.WFVersions.Where(t => t.SaasParam.Equals(SaaSParam)).Max(m => m.Version);
            int MaxCurrentVersion = 0;
            if (maxValuae != null)
                MaxCurrentVersion = maxValuae.Value;
            WFVersion wfVersion = new WFVersion();
            wfVersion.DateCreate = DateTime.Now;
            wfVersion.FilePath = saveAdress;
            wfVersion.Id = Guid.NewGuid();
            wfVersion.SaasParam = SaaSParam;
            wfVersion.Version = MaxCurrentVersion + 1;
            wfVersion.WorkflowName = workflowName;
            wfVersion.WorkflowLevel = workflowLevel;
            dt.WFVersions.Add(wfVersion);
            dt.SaveChanges();
            string folderUpload = ConfigurationManager.AppSettings[ProjectEnum.WORK_FLOW_UPLOAD_FOLDER].ToString();

            CreateMapAndSaveWorkflowflowVersion(folderUpload, wfVersion.FilePath, workflowLevel);
        }

        public IList<WFVersion> GetWorkflows()
        {
            return dt.WFVersions.ToList<WFVersion>();
        }

        public WFVersion GetWorkFlowByLastestVersion(string SaaSParam, string workflowLevel)
        {
            return dt.WFVersions.Where(t => t.SaasParam.Equals(SaaSParam) && t.WorkflowLevel.Equals(workflowLevel)).OrderByDescending(t => t.DateCreate).FirstOrDefault();
        }

        //public string GetXaml(string SaaSParam)
        //{
        //    string folderUpload = ConfigurationManager.AppSettings[ProjectEnum.WORK_FLOW_UPLOAD_FOLDER].ToString();
        //    var item = GetWorkFlowByLastestVersion(SaaSParam);
        //    using (TextReader reader = File.OpenText(folderUpload + item.FilePath))
        //    {
        //        return reader.ReadToEnd();
        //    }

        //}

        //internal string GetXaml(string SaaSParam, out System.Activities.WorkflowIdentity identity)
        //{
        //    string folderUpload = ConfigurationManager.AppSettings[ProjectEnum.WORK_FLOW_UPLOAD_FOLDER].ToString();
        //    var item = GetWorkFlowByLastestVersion(SaaSParam);
        //    int currentVersion = 1;
        //    if(item.Version!= null)
        //        currentVersion = (int)item.Version;
        //    identity = new WorkflowIdentity
        //    {
        //        Name = SaaSParam,
        //        Version = new Version(currentVersion, 0, 0, 0),
        //    }; 

        //    using (TextReader reader = File.OpenText(folderUpload + item.FilePath))
        //    {
        //        return reader.ReadToEnd();
        //    }

        //}
        internal string GetXaml(string SaaSParam, out System.Activities.WorkflowIdentity identity, string workflowLevel)
        {
            string folderUpload = ConfigurationManager.AppSettings[ProjectEnum.HREmployeeICT].ToString();
            // string folderUpload = ConfigurationManager.AppSettings[ProjectEnum.WORK_FLOW_UPLOAD_FOLDER].ToString();
            // 
            //var item = GetWorkFlowByLastestVersion(SaaSParam, workflowLevel);
            int currentVersion = 1;
            //if (item.Version != null)
            //    currentVersion = (int)item.Version;
            identity = new WorkflowIdentity
            {
                Name = SaaSParam,
                Version = new System.Version(currentVersion, 0, 0, 0),
            };
            //D:\ICT Management Project\ICTS.Dashboard\ICTS.WF\Workflow\HREmployeeICT.xaml
            // using (TextReader reader = File.OpenText(folderUpload + item.FilePath))
            using (TextReader reader = File.OpenText(folderUpload))
            {
                return reader.ReadToEnd();
            }

        }
        internal string GetXamlPath(string SaaSParam, string workflowLevel)
        {
            string folderUpload = ConfigurationManager.AppSettings[ProjectEnum.WORK_FLOW_UPLOAD_FOLDER].ToString();
            var item = GetWorkFlowByLastestVersion(SaaSParam, workflowLevel);
            return folderUpload + item.FilePath;
        }

        public DynamicUpdateMap UpdateWorkflowVersion()
        {
            // Load the serialized update map.
            DynamicUpdateMap map;
            string folderUpload = ConfigurationManager.AppSettings[ProjectEnum.WORK_FLOW_UPLOAD_FOLDER].ToString();
            using (FileStream fs = File.Open(folderUpload + @"\MortgageWorkflow.map", FileMode.Open))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(DynamicUpdateMap));
                object updateMap = serializer.ReadObject(fs);
                if (updateMap == null)
                {
                    throw new ApplicationException("DynamicUpdateMap is null.");
                }

                map = (DynamicUpdateMap)updateMap;
            }
            return map;

        }
        private void CreateMapAndSaveWorkflowflowVersion(string folderUpload, string itemPath, string workflowLevel)
        {

            XamlXmlReaderSettings readerSettings = new XamlXmlReaderSettings()
            {
                LocalAssembly = Assembly.GetExecutingAssembly()
            };

            XamlXmlReader xamlReader = new XamlXmlReader(folderUpload + itemPath,
             readerSettings);

            ActivityBuilder ab = XamlServices.Load(
                ActivityXamlServices.CreateBuilderReader(xamlReader)) as ActivityBuilder;

            // Prepare the workflow definition for dynamic update.
            DynamicUpdateServices.PrepareForUpdate(ab);


            // Create the update map.
            DynamicUpdateMap map = DynamicUpdateServices.CreateUpdateMap(ab);

            // Serialize the update map to a file.
            DataContractSerializer serializer = new DataContractSerializer(typeof(DynamicUpdateMap));
            using (FileStream fs = System.IO.File.Open(folderUpload + string.Format(@"\MortgageWorkflow{0}.map", workflowLevel), FileMode.Create))
            {
                serializer.WriteObject(fs, map);
            }
        }
    }
}
