﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using ICTS.Data;

namespace ICTS.WF.Activities
{

    public sealed class TaskActivity<TResult> : NativeActivity<TResult>
    {
        private ICTSDashboardData dbContext = new ICTSDashboardData();

        public TaskActivity() :  base()
        {

        }

        [RequiredArgument]
        public InArgument<string> Swimlance { get; set; }

        [RequiredArgument]
        public InArgument<string> BookmarkName { get; set; } 

        [RequiredArgument]
        public InArgument<string> RoleName { get; set; }


        // Define an activity input argument of type string
        public InArgument<string> Text { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(NativeActivityContext context)
        {
            string projectId = string.Empty;
            // Obtain the runtime value of the Text input argument
            string name = BookmarkName.Get(context);
            context.CreateBookmark(name, new BookmarkCallback(OnResumeBookmark));

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("BookmarkName cannot be an Empty string.", "BookmarkName");
            }

            WFProject project = dbContext.WFProjects.Where(t => t.InstanceId == context.WorkflowInstanceId).FirstOrDefault();
            if (project == null || project.Id.Equals(Guid.Empty.ToString()))
                if (context.DataContext.GetProperties()["ProjectWFId"] != null)
                {
                    projectId = getValue<String>(context, "ProjectWFId").ToString();
                }

            // Get actorId
            string actorId = "";
            string swimlance = Swimlance.Get(context);
            if (!string.IsNullOrEmpty(swimlance))
            {
                if (getValue<String>(context, swimlance) != null)
                    actorId = getValue<String>(context, swimlance ).ToString();
            }

            WFTask task = new WFTask();
            task.Actor = actorId;
            task.DateCreate = DateTime.Now;
            task.Id = Guid.NewGuid();
            task.Status = 0;
            task.TaskName = name;
            task.WFProjectId = Guid.Parse(projectId);
            dbContext.WFTasks.Add(task);

            dbContext.SaveChanges(); 
        }
          
        private object getValue<T1>(NativeActivityContext context, string name)
        {
            var properties = context.DataContext.GetProperties()[name];

            if (properties == null)
            {
                return default(T1);
            }

            return (T1)properties.GetValue(context.DataContext);
        }

        private void setValue(NativeActivityContext context, string name, object value)
        {
            context.DataContext.GetProperties()[name].SetValue(context.DataContext, value);
        }

        // NativeActivity derived activities that do asynchronous operations by calling 
        // one of the CreateBookmark overloads defined on System.Activities.NativeActivityContext 
        // must override the CanInduceIdle property and return true.
        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        public void OnResumeBookmark(NativeActivityContext context, Bookmark bookmark, object obj)
        {
            // Update variable when validate
            if (obj is Dictionary<string, object>)
            {
                Dictionary<string, object> inputs = obj as Dictionary<string, object>;
                foreach (var variable in inputs)
                {
                    setValue(context, variable.Key, variable.Value);
                }
            }
            //   TrackingTaskStatus(context, task);
            // When the Bookmark is resumed, assign its value to
            // the Result argument.
            Result.Set(context, "Validated");
            Console.WriteLine("finish: " + DisplayName);
        }
    }
}
