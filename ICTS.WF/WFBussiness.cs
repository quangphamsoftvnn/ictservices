﻿using System;
using System.Activities;
using System.Activities.XamlIntegration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xaml;
using System.Runtime.DurableInstancing;
using System.Configuration;
using System.Activities.DurableInstancing;
using System.Threading;
using System.Collections.Concurrent;
using ICTS.WF.Version; 
using ICTS.WF.Tracking;
using System.Activities.DynamicUpdate;

namespace ICTS.WF
{
    public class WFBussiness : IWFBussiness
    {
        private  ConcurrentDictionary<Guid, WorkflowApplication> runningWorkflows;
        private  AutoResetEvent synchEvent = new AutoResetEvent(false);
        private WFVersionManager wfVersion = new WFVersionManager();

        #region Private Helper Methods

        private Activity CreateActivityFrom(string xaml)
        {
            var sr = new StringReader(xaml);

            //Change LocalAssembly to where the Activities reside
            var xamlSettings = new XamlXmlReaderSettings
            {
                LocalAssembly = Assembly.GetExecutingAssembly()
            };

            var xamlReader = ActivityXamlServices.CreateReader(new XamlXmlReader(sr, xamlSettings));

            ActivityXamlServicesSettings settings = new ActivityXamlServicesSettings
            {
                CompileExpressions = true
            };

            var result = ActivityXamlServices.Load(xamlReader, settings);

            var activity = result as Activity;

            return activity;
        }

        private  InstanceStore CreateInstanceStore()
        {
            var conn = ConfigurationManager.ConnectionStrings["WorkflowStore"].ConnectionString;

            var store = new SqlWorkflowInstanceStore(conn)
            {
                InstanceLockedExceptionAction = InstanceLockedExceptionAction.AggressiveRetry,
                InstanceCompletionAction = InstanceCompletionAction.DeleteNothing,
                HostLockRenewalPeriod = TimeSpan.FromSeconds(20),
                RunnableInstancesDetectionPeriod = TimeSpan.FromSeconds(3)
            };

            var handle = store.CreateInstanceHandle();

            var view = store.Execute(handle, new CreateWorkflowOwnerCommand(),
                                     TimeSpan.FromSeconds(60));

            store.DefaultInstanceOwner = view.InstanceOwner;

            handle.Free();

            return store;
        }


        #endregion



        #region Manage Workflow Instance
        public Guid StartWorkflowInstance(IDictionary<string, object> inputs, string SaaSParam, string workflowLevel)
        {

            var workflowApp = InitWorkflowApplication(inputs, SaaSParam, workflowLevel);

            workflowApp.Persist();

            var instanceId = workflowApp.Id;

            workflowApp.Run();
            synchEvent.WaitOne();

            //   workflowApp.Unload();
            runningWorkflows.TryAdd(instanceId, workflowApp);

            return workflowApp.Id;
        }
        public Guid StartWorkflowInstanceVersion(IDictionary<string, object> inputs, string SaaSParam, string workflowLevel)
        {

            var workflowApp = InitWorkflowApplication(inputs, SaaSParam, workflowLevel);

            workflowApp.Persist();

            var instanceId = workflowApp.Id;

            workflowApp.Run();
            synchEvent.WaitOne();

            //   workflowApp.Unload();
            runningWorkflows.TryAdd(instanceId, workflowApp);

            return workflowApp.Id;
        }

        private WorkflowApplication InitWorkflowApplication(IDictionary<string, object> inputs, string SaaSParam, string workflowLevel)
        {
            WorkflowIdentity identity;
            string xaml = wfVersion.GetXaml(SaaSParam, out identity, workflowLevel);
            WorkflowApplication workflowApp;
            if (runningWorkflows == null)
            {
                runningWorkflows = new ConcurrentDictionary<Guid, WorkflowApplication>();
            }

            using (System.Transactions.TransactionScope txScope = new System.Transactions.TransactionScope())
            {
                Activity activity = CreateActivityFrom(xaml);
                workflowApp = new WorkflowApplication(activity, inputs, identity)
                {
                    InstanceStore = CreateInstanceStore(),
                    PersistableIdle = OnIdleAndPersistable,
                    Completed = OnWorkflowCompleted,
                    Aborted = OnWorkflowAborted,
                    Unloaded = OnWorkflowUnloaded,
                    OnUnhandledException = OnWorkflowException
                };
                SqlTrackingParticipant track = new SqlTrackingParticipant();
                workflowApp.Extensions.Add(track);
                txScope.Complete();
                txScope.Dispose();

            }
            return workflowApp;
        }

        //public static WorkflowApplication StartWorkflowInstance(IDictionary<string, object> inputs, string SaaSParam)
        //{
        //    WorkflowIdentity identity;
        //    string xaml = workflowVersionManage.GetXaml(SaaSParam, out identity);

        //    if (runningWorkflows == null)
        //    {
        //        runningWorkflows = new ConcurrentDictionary<Guid, WorkflowApplication>();
        //    }
        //    Activity activity = CreateActivityFrom(xaml);

        //    var workflowApp = new WorkflowApplication(activity, inputs, identity)
        //    {
        //        InstanceStore = CreateInstanceStore(),
        //        PersistableIdle = OnIdleAndPersistable,
        //        Completed = OnWorkflowCompleted,
        //        Aborted = OnWorkflowAborted,
        //        Unloaded = OnWorkflowUnloaded,
        //        OnUnhandledException = OnWorkflowException
        //    };
        //    return workflowApp;
        //}

        public  bool LoadInstanceWithBookmark(string bookmarkName,
                                                    Guid instanceId,
                                                    object input,
                                                    string SaaSParam, string workflowLevel)
        {
            if (runningWorkflows == null)
            {
                runningWorkflows = new ConcurrentDictionary<Guid, WorkflowApplication>();
            }

            BookmarkResumptionResult result;

            if (runningWorkflows.ContainsKey(instanceId))
            {
                var workflow = runningWorkflows[instanceId];
                workflow.Completed = OnWorkflowCompleted;
                workflow.PersistableIdle = OnIdleAndPersistable;
                result = workflow.ResumeBookmark(bookmarkName, input, TimeSpan.FromSeconds(60));
            }
            else
            {
                WorkflowIdentity identity;
                string xaml = wfVersion.GetXaml(SaaSParam, out identity, workflowLevel);
                // Setup the persistance
                var store = CreateInstanceStore();

                var activity = CreateActivityFrom(xaml);

                var application = new WorkflowApplication(activity, identity)
                {
                    InstanceStore = store,
                    Completed = OnWorkflowCompleted,
                    Unloaded = OnWorkflowUnloaded,
                    PersistableIdle = OnIdleAndPersistable
                };
                SqlTrackingParticipant track = new SqlTrackingParticipant();
                application.Extensions.Add(track);


                // Update Version by Dynamic Map
                DynamicUpdateMap map = wfVersion.UpdateWorkflowVersion();
                WorkflowApplicationInstance instance = WorkflowApplication.GetInstance(instanceId, application.InstanceStore);
                application.Load(instance, map, TimeSpan.FromSeconds(60));

                //   application.Load(instanceId, TimeSpan.FromSeconds(60));
                result = application.ResumeBookmark(bookmarkName, input, TimeSpan.FromSeconds(60));

                runningWorkflows.TryAdd(instanceId, application);
            }
            synchEvent.WaitOne();

            return result == BookmarkResumptionResult.Success;
        }

        public  void UnloadInstance(Guid instanceId)
        {
            if (!runningWorkflows.ContainsKey(instanceId))
            {
                return;
            }

            var workflow = runningWorkflows[instanceId];
            workflow.Unload();

            runningWorkflows.TryRemove(instanceId, out workflow);
        }

        public  void TerminalProject(Guid WorkflowInstanceId, IDictionary<string, object> inputs, string SaaSParam, string workflowLevel)
        {
            if (!WorkflowInstanceId.Equals(Guid.Empty))
            {
                if (runningWorkflows == null)
                {
                    runningWorkflows = new ConcurrentDictionary<Guid, WorkflowApplication>();
                }
                WorkflowApplication application;
                if (runningWorkflows.ContainsKey(WorkflowInstanceId))
                {
                    application = runningWorkflows[WorkflowInstanceId];
                    application.Completed = OnWorkflowCompleted;
                    application.PersistableIdle = OnIdleAndPersistable;
                    application.Unloaded = OnWorkflowUnloaded;
                }
                else
                {
                    WorkflowIdentity identity;
                    string xaml = wfVersion.GetXaml(SaaSParam, out identity, workflowLevel);

                    // Setup the persistance
                    var store = CreateInstanceStore();

                    var activity = CreateActivityFrom(xaml);

                    application = new WorkflowApplication(activity, identity)
                    {
                        InstanceStore = store,
                        Completed = OnWorkflowCompleted,
                        Unloaded = OnWorkflowUnloaded,
                        PersistableIdle = OnIdleAndPersistable
                    };
                }
                // Update Version by Dynamic Map
                DynamicUpdateMap map = wfVersion.UpdateWorkflowVersion();

                WorkflowApplicationInstance instance = WorkflowApplication.GetInstance(WorkflowInstanceId, application.InstanceStore);
                application.Load(instance, map, TimeSpan.FromSeconds(60));
                application.Terminate("Delete");

            }
        }
        #endregion

        #region Events

        private  void OnWorkflowCompleted(WorkflowApplicationCompletedEventArgs e)
        {
            if (runningWorkflows != null && runningWorkflows.ContainsKey(e.InstanceId))
            {
                WorkflowApplication workflowApp;
                runningWorkflows.TryRemove(e.InstanceId, out workflowApp);
                //if (e.CompletionState == ActivityInstanceState.Faulted)
                //{
                //    Console.WriteLine("Workflow {0} Terminated.", e.InstanceId);
                //    Console.WriteLine("Exception: {0}\n{1}",
                //        e.TerminationException.GetType().FullName,
                //        e.TerminationException.Message);
                //}
                //else if (e.CompletionState == ActivityInstanceState.Canceled)
                //{
                //    Console.WriteLine("Workflow {0} Canceled.", e.InstanceId);
                //}
                //else
                //{
                //    Console.WriteLine("Workflow {0} Completed.", e.InstanceId);

                //    // Outputs can be retrieved from the Outputs dictionary,
                //    // keyed by argument name.
                //    Console.WriteLine("The two dice are {0} and {1}.",
                //        e.Outputs["D1"], e.Outputs["D2"]);
                //}
            }
            synchEvent.Set();
        }

        private  PersistableIdleAction OnIdleAndPersistable(WorkflowApplicationIdleEventArgs e)
        {
            synchEvent.Set();
            return PersistableIdleAction.Unload;

        }

        private  void OnWorkflowAborted(WorkflowApplicationAbortedEventArgs e)
        {
            //Console.WriteLine(e.Reason);
        }

        private  void OnWorkflowUnloaded(WorkflowApplicationEventArgs e)
        {
            if (runningWorkflows != null && runningWorkflows.ContainsKey(e.InstanceId))
            {
                WorkflowApplication workflowApp;
                runningWorkflows.TryRemove(e.InstanceId, out workflowApp);
            }
        }

        private  UnhandledExceptionAction OnWorkflowException(WorkflowApplicationUnhandledExceptionEventArgs e)
        {
            //log the exception here using e.UnhandledException 
            return UnhandledExceptionAction.Terminate;
        }

        #endregion

    }
}
