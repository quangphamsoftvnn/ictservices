﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ICTS.WF
{
    public static class Helpers
    {
        public static string WORKFLOW_STORE = "WorkflowStore";
    }

    public class ProjectEnum
    {
        public static string PATH_UPLOAD_WORKFLOW = @"~\ManageWorkFlow\Upload\";
        public static string PATH_UPLOAD_WORKFLOW_FOLDER = @"\ManageWorkFlow\Upload\";
        public static string WORK_FLOW_UPLOAD_FOLDER = "WorkflowUploadFolder";
        public const string AUCHAN = "Auchan";
        public const string WORKFLOW_STORE = "WorkflowStore"; 
        public const string HREmployeeICT = "HREmployeeICT";
    }
    public class ProjectHelper
    {

        public static string ConvertByteToStringHTML(byte[] serializedDataProperties)
        {
            return ReadDataProperties(serializedDataProperties, true);
        }
        public static string ReadDataProperties(byte[] serializedDataProperties, bool isCompressed)
        {
            if (serializedDataProperties != null)
            {

                using (MemoryStream memoryStream = new MemoryStream(serializedDataProperties))
                {
                    // if the instance state is compressed using GZip algorithm
                    if (isCompressed)
                    {
                        // decompress the data using the GZip 
                        using (GZipStream stream = new GZipStream(memoryStream, CompressionMode.Decompress))
                        {
                            // create an XmlReader object and pass it on to the helper method ReadPrimitiveDataProperties
                            using (XmlReader reader = XmlDictionaryReader.CreateBinaryReader(stream, XmlDictionaryReaderQuotas.Max))
                            {
                                // invoke the helper method
                                //ReadPrimitiveDataProperties(reader, propertyBag);
                                if (reader.Read())
                                {
                                    return reader.ReadOuterXml();
                                }
                            }
                        }
                    }
                    else
                    {
                        // if the instance data is not compressed 
                        // create an XmlReader object and pass it on to the helper method ReadPrimitiveDataProperties
                        using (XmlReader reader = XmlDictionaryReader.CreateBinaryReader(memoryStream, XmlDictionaryReaderQuotas.Max))
                        {
                            // invoke the helper method
                            //ReadPrimitiveDataProperties(reader, propertyBag);
                            if (reader.Read())
                            {
                                return reader.ReadOuterXml();
                            }
                        }
                    }
                }
            }

            return null;
        }

    }
}
