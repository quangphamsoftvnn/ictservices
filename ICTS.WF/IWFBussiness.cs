﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICTS.WF
{
    public interface IWFBussiness
    { 
        bool LoadInstanceWithBookmark(string bookmarkName, Guid instanceId, object input, string SaaSParam, string workflowLevel);
        Guid StartWorkflowInstanceVersion(IDictionary<string, object> inputs, string SaaSParam, string workflowLevel);
        void TerminalProject(Guid WorkflowInstanceId, IDictionary<string, object> inputs, string SaaSParam, string workflowLevel);
        void UnloadInstance(Guid instanceId);
        //Guid StartWorkflowInstance(IDictionary<string, object> inputs, string SaaSParam);
        Guid StartWorkflowInstance(IDictionary<string, object> inputs, string SaaSParam, string worflowVersion);
    }
}
